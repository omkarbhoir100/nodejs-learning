module.exports.calc = async (num) => {

  
    let fact = await factorial(num);
    let fibb = await fibonacci(num);
    let number = await evenOrOdd(num);
    let isPrime = await isPrimeNumber(num);
    
    if(fibb != undefined) console.log('fibb');

    let res = {
        number : number,
        is_prime : isPrime,
        factorial : fact,
        fibonacci : fibb
    }
    return new Promise((resolve, reject) => {
        resolve(res);
    });
}

function fibonacci(num){

    // let a = c = inc = 0;
    // let b = 1;

    // while(inc <= num) {
    //     c = a+b;
    //     a = b;
    //     b = c;
    //     inc++;
    // }

    // if(c != 0) console.log(c)
    // return c;

    if(num <2) {
        return 1;
    } else {
        return fibonacci(num-1)+fibonacci(num-2);
    }
}

async function evenOrOdd(num) {

    let res = undefined;
    if(num > 2 && num%2 !== 0) {
        res = 'odd';
    } else {    
        res = 'even';
    }
    if(res != undefined) console.log('even or odd');
    return res;
}

async function isPrimeNumber(num) {

   if(num == 1) return true;
   
    var prime = undefined;
    let inc = num-1;
    while(inc >= 2) {
        if(num%inc == 0) {
            prime = false;
            break;
        } else {
            prime = true
        }  
        inc--;
    }

    if(prime != undefined) console.log('prime');
    return prime;
}

function factorial(num){

    if(num == 1) return 1;

    let fact = 1;
    while(num >= 2) {
        fact = num*fact;
        num--;
    }

    if(fact > 1) console.log('fact');
    return fact;
}