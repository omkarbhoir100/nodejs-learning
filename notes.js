// a(); //* This will be executed 
// b(); //! This will throw an error beacause it will have value undefined during hosting phase memory allocation phase

//? Function Statement aka Declaration
function a() {
    console.log("a called");
}

//? Fun nd Anonymous Function
var b = function () {
    console.log("b called");
}

//? Named function expression
var z = function x(){

}

//? Anonymous Function
//! This will throw error beacause function statement require function name but we can use anonymous function as expression statment
// function () {

// }

//? Difference between parameters and aruguments
function abc (param1, param2) {

}
abc(argument1 = 1, argument2 = 2)

//? First Class functions aka First Class Citzens -- Ability to be used like values that can be passed to another function and can be return as function

function fun1(param) {
    console.log(param());
}

function fun2() {
    console.log('inside fun2');
    return 'returning from fun2';
}

function fun3 () {
    return function () {
        console.log('inside fun3');
        return 'returning from fun3'
    }
}

fun1(fun2);
console.log(fun3()())


//? Arrow Functions - ES6
// var myfun = () => {
//     console.log('This is arrow function');
// };  
// myfun();


//? Temporal dead zone in javascript -> When we execute or call any function or variable before declaring in javascript is called temporal dead zone
//? Syntax error , Type error, Reference error
//? Hoisting is phase of phenoma in javascript where we can access variable and function before intialization this is limited to the global context.
//? Block Scope in javascript has different scopes Global , Local ,Block and Script 

/**
 *  Java Script runtime use Mark and Sweep algorithm for garbage collection
 *  Java Script can be both interpreted and compiled depends on different JS engines it use JIT compiler 
 *  in which code in interpreted line by line and then compilier tries optimize the code and generated 
 *  byte code and pass to execution phase it doesn't create class file unlike java this process happens 
 *  on everytime of code execution
 *  JS code break into CODE -> PARSING /SYNTAX PARSING -> COMPILATION -> EXECUTION
 *  1) CODE - Javascript code 
 *  2) PARSING - Coverts the code into AST (abstract syntax tree)
 *  3) COMPILATION -> (inline,copy elisian, inline caching) AST is interpreted and compliled line by using JIT compiler
 *  4) EXECUTION -> Byte code is passed for execution and memory allocation (HeapMemory and Call stack)
 * 
 *  V8 has interpretter called Ingition and Compiler called turbo fan (optimising compiler) they make
 *  code to run very fast
 *  Read about V8 engine its very intresting. 
 * 
 *  Scope Chain and lexical Scope 
 *
 *  Mark and Sweep Algorithm
 *  This Garbage Collector has two Phase Mark and Sweep and this is called as tracing algorithm
 *  In Mark Phase 
 *      The tracer starts tracing the live object from the root to the end of the heap live object are allocated like a linked list 
 *      there is always pointer to the next live object from the root or from preivous live object and we mark thoes live object
 *  In Sweep phase
 *      The relcaim of the thoes of object begins from the root to end of the heap it check weather the object is marked as live object
 *      if yes then it goes to the next object and reset the marked for next iteration else it deallocate the object and mark it as 
 *      free 
 *  
 * Summary of this algorithm 
 * This is the tracing alogrithm
 * It has two phases Mark (trace) and Sweep (Reclaim)
 * It in non moveable objects hence it suffer from slow memory allocation
 * Mostly used in language having pointer premitives like C and C++
 * Fragmentation of the memory is the issue over here and can be solved using Mark Compact Collector
 * 
 * 
 * JS DOM event and Event Bubbling and Capturing
*/

/**
 * What is middleware ?
 *  Middleware functions are functions that have access to the request object ( req ), the response object ( res ) The next function is a function which,  
 *  when invoked, executes the succeeding (next) middleware.
 * 
 * Middleware functions can perform the following tasks:
 *  Execute any code.
 *  Make changes to the request and the response objects.
 *  End the request-response cycle.
 *  Call the next middleware in the stack.
 * 
 * Types of express middleware
 *  Application level middleware app.use
 *  Router level middleware router.use
 *  Built-in middleware express.static,express.json,express.urlencoded
 *  Error handling middleware app.use(err,req,res,next)
 *  Thirdparty middleware bodyparser,cookieparser
 * 
 * What is Error Handling Middleware?
 *  In error handling middleware first parameter is an error object. (err, req, res, next)
 *  @link https://stackoverflow.com/questions/14125997/difference-between-app-all-and-app-use
 */

 /**
  * Difference betweeen router.use and app.use
  * 
  */


/**
 * What is hoisting in Javascript
 * Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their scope before code execution. 
 * eg:
 * var a = 10;
 * let b = 20;
 * function abc() {
 *      var c = 30;
 *      let d = 40;
 *      if(true) {
 *          var e = 50;
 *          let f = 60;
 *      }
 * }
 * 
 *  Variable a c e and function abc() will be hoisted to the global execution context.
 */

/** 
 * what are promise in Javascript
 * Promises are use to handle the asynchronous operations. They provide an alternative approach for callbacks and avoid the problems caused by
 * callback such as callback hell and helps writing the cleaning code 
 * Promises has three different state
 * 1. Pending : This is intial state of the operations
 * 2. Fulfilled : This state indicates that operation has completed
 * 3. Rejected : This state indicates that operation did not completed.
 * Different Promise handlers
 * promise.race() eg racing between loader, userData and cancel button
 * promise.all()  eg Wait tills all the promise get completed or any one of the get rejected.
 * promise.any()  eg Wait to settle any one of the promise or any one of the get rejected.
 * 
*/

/**
 * What is callback in Javascript
 * A callback function in a function passed to another function as an argument, This function is invocked inside the outer function to complete
 * the other actions.
 * The callback are neeeded beacause javascript is event driven. That means instead of waiting for the response the javascript will keep executing 
 * and listining the other events
 * 
 */

/** 
 * What is temporal deadzone
 * Temporal dead zone occurs when declaring variable with let and const not with the var, And accessing let and const variable before declaring
 * causes a ReferenceError, The timespan between creation of the variable's binding and declaration is called temporal dead zone.
 * 
*/

/**
 * What is Scpoe in Javascript
 * Scpoe determines the visibility of variables and other resources in areas of your code during runtime.
 */

/**
 * What is strict mode in javascript
 * Strict mode is usefull to write "secure" Javascript by notifing "bad syntax" into real error
 * 1. Eliminates accidentally creatning global variable by throwing error
 * 2. Error for assignment for non writeable properties, only getter property or non-existance object.
 */

/**
 * What is async and await
 * The async and await keywords enable asynchronous, promise-based behavior to be written in a cleaner 
 * style, avoiding the need to explicitly configure promise chains.
 * use instead of promise channing
 */