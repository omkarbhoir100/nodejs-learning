const os = require('os');

//? Return platform
console.log(os.platform());

//? Return Arictecture
console.log(os.arch())

//? Return details of CPUs core as object
console.log(os.cpus())

//? Free memory
console.log(os.freemem());

//? Total memory
console.log(os.totalmem());

//? Home dir
console.log(os.homedir());

//? Show uptime
console.log(os.uptime());

//? Show network intertface
console.log(os.networkInterfaces());