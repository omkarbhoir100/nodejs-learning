const url = require('url');

const pageUrl = new URL('https://www.pepperfry.com/three-seater-sofas.html?p=2&cat=3&variation=2342423');


//? Serialized url
console.log(pageUrl.href);
console.log(pageUrl.toString())

//? Hostname
console.log(pageUrl.host);
console.log(pageUrl.hostname);

//? Pathname
console.log(pageUrl.pathname);

//? Serialized query
console.log(pageUrl.search);

//? Search params
console.log(pageUrl.searchParams);

//? Append search params
pageUrl.searchParams.append('is_bespoke','true');
console.log(pageUrl.searchParams);

//? Looping through parameters
pageUrl.searchParams.forEach((value, name) => {
    console.log(value,name);
})