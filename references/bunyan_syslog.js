var bunyan = require('bunyan');
var bsyslog = require('bunyan-syslog');


var log = bunyan.createLogger({
	name: 'foo',
	streams: [ {
		level: 'info',
		type: 'raw',
		stream: bsyslog.createBunyanStream({
			type: 'sys',
			facility: bsyslog.local0,
			host: '192.168.0.1',
			port: 514
		})
	}]
});

log.info(` @cee: {
    "requestid": "1270011613979265330",
    "program": "API",
    "platform": "APP",
    "proctime": 0.1353,
    "data": "",
    "session": "6f00e0facf6001ed31add92a29a062134c559d166da75157ee4e461cf0e9bb8c",
    "request_url": "\/v2\/ar\/session\/create",
    "function": "handle",
    "event": "TOKEN_MIDDLEWARE"
    }`);
// Feb 27 13:35:18 developer node: {"name":"foo","hostname":"developer","pid":11272,"level":30,"foo":"bar","msg":"hello world","time":"2021-02-27T08:05:18.603Z","v":0}