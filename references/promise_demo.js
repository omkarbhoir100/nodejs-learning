/** Promise based instead of callbacks starts */
function foo(msg) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('Hello Omkar') 
        }, 2000);
    });
}

function bar(msg, cb) {
    setTimeout(() => {
        cb(`Welcome back ${msg}...`)
    },1000);
}

function baz() {
    return `This is callback example!`
}


async function demo() {
    var foobar = await foo('Omkar');
    console.log(foobar);

    /** Write this code will block the execution will work as sync tread this function as callback function only */
    bar('Omkar', (data) => {
        console.log(data);
    })
    var res = baz();
    console.log(res);
}

/** Write this code here will make this function like async run the code to see the results */
bar('Omkar', (data) => {
    console.log(data);
})
var res = baz();
console.log(res);


demo();
/** Promise based instead of callbacks ends */

/**
 * Promsies and its different states
 * returning promises
 * promises as expression
 * promies all
 * promises .then .catch
 * 
 */
