/** Effective use of callbacks starts async */

function foo(msg, cb) {
    setTimeout(() => {
        cb(`Hello ${msg}`);
    }, 2000)
}

function bar(msg, cb) {
    setTimeout(() => {
        cb(`Welcome back ${msg}...`)
    },1000);
}

function baz() {
    return `This is callback example!`
}


foo('Omkar' , (data) => {
    console.log(data);
})

bar('Omkar', (data) => {
    console.log(data);
})

var res = baz();
console.log(res);

/** Effective use of callbacks ends async */


/** Effective use of callbacks starts sync*/
function foo(msg) {
    delay(1000);
   console.log('Hello Omkar');
}

function bar(msg) {
    delay(2000);
   console.log('Welcome back Omkar');
}
    
function baz() {
   console.log('This is callback example');
}


foo();
bar();
baz();
/** Effective use of callbacks ends sync */