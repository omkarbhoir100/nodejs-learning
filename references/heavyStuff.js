process.on('message', (message) => {
    const result = fibb(message);
    process.send(result);
    process.exit();
}); 

function fibb(number) {
    if(number <2) {
        return 1;
    } else {
        return fibb(number-1)+fibb(number-2);
    }
}

exports.fibb = (number) => {
    if(number <2) {
        return 1;
    } else {
        return fibb(number-1)+fibb(number-2);
    }
}