const EventEmitter = require('events');

//? Create class
class myEventEmitter extends EventEmitter{}

//? Init Object
const myEvent = new myEventEmitter();

//? Event listener
myEvent.on('init', () =>{
    console.log('Event Initailized!')
});

myEvent.on('success', () =>{
    console.log('Event Successed!')
});

myEvent.on('error', () =>{
    console.log('Event has error!')
});

myEvent.on('log', () =>{
    console.log('Event has logged!')
});


//? Init events
myEvent.emit('init');
myEvent.emit('success');
myEvent.emit('error');
myEvent.emit('log');
