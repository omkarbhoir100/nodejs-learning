var taskCompleted = true;

/** Return the promise instance */
function foo() {
    return new Promise((resolve, reject) => {
        // Async task 
        setTimeout(() => {
            if(taskCompleted) {
                resolve('Fullfiled');
            } else {
                reject('Rejected');
            }
        },2000)
    });
}

let response = foo();
response.then((value) => {
    console.log(value);
}).catch((error) => {
    console.log(error);
});