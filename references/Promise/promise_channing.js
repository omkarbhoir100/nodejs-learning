function getUrlData() {
    return new Promise((resolve,reject) => {
        setTimeout(() => {
            resolve('Done URL Data');
            console.log('Feteched URL Data');
        }, 8000)
    });
}

function getProcessData(urlData) {
    return new Promise((resolve, reject) => {
        // console.log(urlData)
        if(urlData != undefined) {
            setTimeout(() => {
                resolve('Done Processing');
                console.log('Done Processing');
            }, 12000)
           
        } else{
            reject('Error while processing');
        }
    });
}

function setCache(processData) {
    return new Promise((resolve, reject) => {
        if(processData != undefined) {
           setTimeout(() => {
            resolve('Cache Set');
            console.log('Cache Set');
           }, 8000)
        } else {
            reject('Error while caching');
        }
    });
}

function abc() {
    console.log('inside abc');
}

/** Performing async operations */
// function main() {
//     let urlData = getUrlData();
//     let processData = getProcessData(urlData);
//     setCache(processData);
//     console.log('Done Task');
//     // Return the response
//     return 0;
// }
// main();

/** Fixed above issue using promise.chaining() */
// function main() {
//     let urlData = getUrlData();
//     urlData.then((value) => {
//         console.log('Done Processing');
//     }).then((value) => {
//         console.log('Cache Set');
//     }).then((value)=> {
//         console.log('Done Task');
//     }).catch((error) => {
//         console.log(error)
//     });
//     // Return the response
//     return 0;
// }
// main();

/** Async and Await instead of promise chaining */
async function main() {
    abc();
    let urlData =   getUrlData();
    console.log(urlData) ;
    let processData = await getProcessData(urlData);
    await setCache(processData);
    console.log('Done Task');
    // Return the response
     
    return 0;
}
main();