function foo() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('Done foo');
        },2000);
    });
}
function bar() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('Done bar');
        },4000);
    });
}
function baz() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('Done baz');
        },1000);
    });
}


function main() {

    /** Will wait to complete all the promise or any reject promise */
    Promise.all([foo(), bar(),baz()]).then((value) => {
        console.log(value)
    }). catch((error) => {
        
    });

     /** Will wait to complete any one promise or any reject promise */
    // Promise.any([foo(), bar(),baz()]).then((value) => {
    //     console.log(value)
    // }). catch((error) => {
        
    // });

     /** Settles with first completed promise or any rejected promise */
    Promise.race([foo(), bar(),baz()]).then((value) => {
        console.log(value)
    }). catch((error) => {
        
    });
}

main();