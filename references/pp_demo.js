const crypto = require('crypto');
const performance = require('perf_hooks');

const arr = new Array(100).fill(30);
var psWhole = [];
var psChunk = [];
let inc = 0;
function processChunck(){
    var start = Date.now();
    console.log(start)
    if(arr.length === psChunk.length){
        console.log('array processing has been ended')
        var end =  Date.now();
        console.log(`Execution time: ${end - start} ms`);
        console.log(psChunk);
    } else {
        console.log('processing chunck');
        const subarr = arr.splice(inc,25);
        inc = inc+5;
        for(const item of subarr){
           let fact = doHeavyStuff(item);
           psChunk.push(fact)
        }
        setImmediate(processChunck)
    }
}

function processWhole() {
    var start = Date.now();
    for(const item of arr) {
        let fact = doHeavyStuff(item);
        psWhole.push(fact)
    }
    var end = Date.now();
    console.log(`Execution time: ${end - start} ms`);
    console.log(psWhole)
}

processWhole();
// processChunck();

function doHeavyStuff(num){
    return fib(num)
    // crypto.createHmac('sha256', 'secret').update(new Array(10000).fill(item).join(' . ')).digest('hex')
}

function fib(num) {
    if(num <2) {
        return 1;
    } else {
        return fib(num-1)+fib(num-2);
    }
}