const path = require('path');

//? Fiilpath 
console.log(__filename);

//? Base file name
console.log(path.basename(__filename));

//? Dir path 
console.log(__dirname);

//? Dir name
console.log(path.dirname(__filename));

//? Get file extexnions
console.log(path.extname(__filename));

//? Create path object
console.log(path.parse(__filename));
console.log(path.parse(__filename).base);  // Can be access by properties root, ext, name, dir 

//? Concatenate path 
console.log(path.join(__dirname, 'test', 'text.js'));