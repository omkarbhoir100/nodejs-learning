const fs = require('fs');

function step(iteration)
{
    if (iteration === 10) return;
    fs.readFile(__filename,() => {
        setTimeout(() => {
            console.log(`setTimeout iteration: ${iteration}`);
        },0);
        setImmediate(() =>
        {
            console.log(`setImmediate iteration: ${iteration}`);
            step(iteration + 1);  // Recursive call from setImmediate handler.
            
        });
        process.nextTick(() =>
        {
            console.log(`nextTick iteration: ${iteration}`);
        });
    });
}
step(0);