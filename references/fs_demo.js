const fs = require('fs');
const path = require('path');

/**
 * FS module has two kinds of implementations sync and async. Async contains additional paramter called callback 
 * default we use async methods.
 */

// ? Create new dir
// fs.mkdir(path.join(__dirname,'/test'), {}, (err) => {
//     if(err) throw err;
//     console.log('Folder Created...');
// });

//? Create and write to new file
// fs.writeFile(path.join(__dirname,'/test', 'hello.txt'), 'Hello world!!', (err) => {
//     if(err) throw err;
//     console.log('File Created and Written to...');
// });

// ! This will override the file content
// fs.writeFile(path.join(__dirname,'/test', 'hello.txt'), 'I love nodeJS', (err) => {
//     if(err) throw err;
//     console.log('File Created and Written to...');
// });


// ? Create and write to file
// fs.writeFile(path.join(__dirname,'/test', 'hello.txt'), 'Hello world!!', (err) => {
//     if(err) throw err;
//     console.log('File Created ...');

//     //? Append to existing file
//     fs.appendFile(path.join(__dirname,'/test', 'hello.txt'), 'I love Node js ;)', (err) => {
//         if(err) throw err;
//         console.log('File Written to...');
//     });
// });

//* Note: Here we can see example of async programming were renane get executed first and then readfile without any error due to closure*/

//? Read file
// fs.readFile(path.join(__dirname,'/test', 'hello.txt'), 'utf8', (err, data) => {
//     if(err) throw err;
//     console.log('Reading file...');
//     console.log(data);
// }); 

//? Rename File
// fs.rename(path.join(__dirname,'/test', 'hello.txt'), path.join(__dirname,'/test', 'demo.txt'), (err) => {
//     if(err) throw err;
//     console.log('File renamed');
// });