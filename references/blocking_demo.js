const heavyStuff = require('./heavyStuff.js');
const cp = require('child_process');

module.exports.run = (num) => {

    /** Blocking code */
    // return new Promise((resolve, reject) => {
    //     const res = heavyStuff.fibb(num);
    //     resolve(res);
    // });

    /** Non-blocking code  */
    if(num > 30) {
        const subProcess = cp.fork('./references/heavyStuff.js');
        subProcess.send(parseInt(num))
        return new Promise((resolve, reject) => {
            subProcess.on('message', (message) => {
                resolve(message);
            }); 
        });
        
    } else {
        return new Promise((resolve, reject) => {
            const res = heavyStuff.fibb(num);
            resolve(res);
        });
    }
}