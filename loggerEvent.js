const eventEmitter = require('events');
const uuid = require('uuid'); //? External package that generates Cryptographically-strong random values unique ids
const fs = require('fs');
const path = require('path');

class LoggerEvent extends eventEmitter {

    log(msg){
        //? Firing event
        this.emit('message',{id:uuid.v4(),msg});
    }

    pushEvent(data) {
        fs.access(path.join(__dirname,'/syslogs', 'appLogs.txt'), 'fs.constants.F_OK',(err) => {
            if(err) {
                fs.mkdir(path.join(__dirname,'/syslogs'), (err) => {
                    if(err) {
                        console.log(err)
                        throw Error('Error while creating file...')
                    } else { 
                        fs.writeFile(path.join(__dirname,'/syslogs', 'appLogs.txt'), `App Log ID : ${data.id} ${data.msg} \n`, (err) => {
                            if(err) throw Error('Error while writing file...')
                        });
                    }
                })
            } else {
                fs.appendFile(path.join(__dirname,'/syslogs', 'appLogs.txt'), `App Log ID : ${data.id} ${data.msg} \n`, (err) => {
                    if(err) throw Error('Error while writing file...')
                });
            }
        })
        // console.log(`_logId : ${data.id}`);
    }
}

const loggerEvent = new LoggerEvent();

//? Event Listiner 
loggerEvent.on('message', (data) => {
    loggerEvent.pushEvent(data)
});

module.exports = loggerEvent;