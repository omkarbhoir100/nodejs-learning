module.exports.run = (num) => {
    let res = {
        number : evenOrOdd(num),
        is_prime : isPrimeNumber(num),
        factorial : factorial(num),
        fibonacci : fibonacci(num)
    }

    return res;
}

function fibonacci(num){

    // let a = c = inc = 0;
    // let b = 1;

    // while(inc <= num) {
    //     c = a+b;
    //     a = b;
    //     b = c;
    //     inc++;
    // }

    // return c;

    if(num <2) {
        return 1;
    } else {
        return fibonacci(num-1)+fibonacci(num-2);
    }
}

function evenOrOdd(num) {

    if(num > 2 && num%2 !== 0) {
        return 'odd';
    } else {    
        return 'even';
    }
}

function isPrimeNumber(num) {

   if(num == 1) return true;
   
    var prime = true;
    let inc = num-1;
    while(inc >= 2) {
        if(num%inc == 0) {
            prime = false;
            break;
        }   
        inc--;
    }

    return prime;
}

function factorial(num){

    if(num == 1) return l;

    let fact = 1;
    while(num >= 2) {
        fact = num*fact;
        num--;
    }

    return fact;
}

