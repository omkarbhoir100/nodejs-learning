const http = require('http');
const url = require('url');
const loggerEvent = require('./loggerEvent');
const PORT = process.env.PORT || 5000

//? Created new http server
http.createServer((req, res) => {

    const queryObject = url.parse(req.url,true)

    if(queryObject.pathname === '/'){
        res.writeHead(200, { 'Content-Type': 'text/html' })
        res.write('<h1>Node Js walk through</h1>')
        res.end()
    }
    
    if(queryObject.pathname === '/api/blocking') {
        const code = require('./references/blocking_demo');
        let output = code.run(queryObject.query.num);
        output.then((result) => {
            res.writeHead(200, { 'Content-Type': 'application/json' })
            res.write(JSON.stringify(result))
            res.end()
        });
    }

    if(queryObject.pathname === '/async/api') {
        const script = require('./async/async');
        let output = script.calc(20);
        output.then((result) => {
            res.writeHead(200, { 'Content-Type': 'application/json' })
            res.write(JSON.stringify(result))
            res.end()
        })
    }

    if(queryObject.pathname === '/sync/api') {
        const script = require('./sync/sync');
        let output = script.run(10);
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.write(JSON.stringify(output))
        res.end()
    
    }

    loggerEvent.log('App Request');

}).listen(PORT, () => console.log(`Server running on ${PORT}...`));






